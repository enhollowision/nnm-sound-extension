'use strict';

// Config
const pluginName = 'Extension-HP-NNM-Sounds.';
let iterationCounter = 0;
//http://gldb-nnm.dvfrmp.ru/nnm/main
const nnmPath = 'http://gldb-nnm.dvfrmp.ru/nnm/';
const delay = 15000;
const schemeAll = 'topo_%D0%A1%D0%A5%D0%95%D0%9C%D0%90+%D0%9E%D0%91%D0%A9%D0%90%D0%AF';

console.log(pluginName, 'Content script started.');

const convertToParameters = function (data) {
    if (data) {
        const query = [];
        for (const key in data) {
            query.push(encodeURIComponent(key) + '=' + encodeURIComponent(data[key]));
        }
        return query.join('&');
    } else {
        return null;
    }
};
const Ajax = {};
Ajax.Request = function (url, method, data, callback) {
    method = method.toUpperCase();
    if (method !== 'GET' && method !== 'POST') {
        throw 'Unknown method type: ' + method;
    }
    const request = new XMLHttpRequest();
    request.onload = function () {
        if (request.status >= 200 && request.status < 400) {
            const resp = request.responseText;
            callback(null, resp);
        } else {
            // We reached our target server, but it returned an error
            callback(request.status);
        }
    };
    request.onerror = function () {
        // There was a connection error of some sort
        callback('Unexpected error!');
    };
    const parameters = convertToParameters(data);
    if (method === 'GET' && parameters) {
        url += ('?' + parameters);
    }
    request.open(method, url, true);
    if (method === 'POST') {
        request.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
        request.send(parameters);
    } else {
        request.send();
    }
};
Ajax.Get = function (url, data, callback) {
    Ajax.Request(url, 'GET', data, callback);
};
Ajax.Post = function (url, data, callback) {
    Ajax.Request(url, 'POST', data, callback);
};
// Help objects
const VanillaJs = {Ajax: Ajax};

const isFirefox = (typeof browser !== 'undefined');
window.browser = (typeof browser !== 'undefined' ? browser : chrome);

// Sound server
const createAudioElement = function (d) {
    console.log(pluginName, 'Creating audio element');
    const soundElement = d.createElement('audio');
    soundElement.setAttribute('type', 'audio/ogg');
    soundElement.setAttribute('src', browser.runtime.getURL('resources/tada.ogg'));
    d.body.appendChild(soundElement);
    return soundElement;
};

const soundDriver = {
    '_audioElem': null,
    "_init": function () {
        this._audioElem = createAudioElement(document);
        this.playAlert = this._play;
        this.playAlert();
    },
    '_play': function () {
        this._audioElem.play();
    }
};
soundDriver.playAlert = soundDriver._init;

const successNnmDataReceived = function (data) {
    console.log(pluginName, 'NNM data was successfully received! Iteration', iterationCounter);
    const responseHandle = function (response) {
        if (response.type !== 'should_alert') {
            throw 'API mismatch';
        }
        const shouldAlert = response.data;
        if (shouldAlert) {
            console.log(pluginName, 'alerting.');
            soundDriver.playAlert();
        } else {
            console.log(pluginName, 'no need in alert.');
        }
    };
    if (typeof data.MapView !== 'undefined' && typeof data.MapView.nodeList !== 'undefined') {
        if (isFirefox) {
            browser.runtime.sendMessage({
                'type': 'should_alert',
                'nodes': data.MapView.nodeList
            }).then(responseHandle, (err) => {
               console.error(pluginName, 'browser.runtime.sendMessage', err);
            });
        } else {
            browser.runtime.sendMessage({
                'type': 'should_alert',
                'nodes': data.MapView.nodeList
            }, responseHandle);
        }
    } else {
        console.error(pluginName, 'incomplete node list.');
    }
};

const errorNnmDataReceived = function (errorData) {
    console.error(pluginName, 'NNM data was not received! Iteration ', iterationCounter);
    console.error(errorData);
};

const get_cookie = function (cookie_name) {
    const regExString = '(^|;) ?' + cookie_name + '=([^;]*)(;|$)';
    const results = document.cookie.match(regExString);
    if (results && results.length > 3) {
        return decodeURIComponent(results[2]);
    }
    return null;
};


let viewId = '';
let nodegroup = '';

const thread = function () {
    ++iterationCounter;
    const data = {
        'viewId': viewId,
        'viewInfoId': schemeAll,
        'isReadOnly': false,
        'w': 1920,
        'h': 1200,
        'nodegroup': nodegroup
    };
    VanillaJs.Ajax.Post(nnmPath + 'mapview', data, function (err, result) {
        if (err) {
            errorNnmDataReceived(err);
        } else {
            successNnmDataReceived(JSON.parse(result));
        }
        setTimeout(thread, delay);
    });
};

const successGetId = function (data) {
    console.log(pluginName, 'Id was successfully received.');
    viewId = data.viewId;
    nodegroup = data.nodegroup;
    thread();
};

const errorGetId = function (errorData) {
    console.error(pluginName, 'Error! Id was not received');
    console.error(errorData);
    ready();
};

const main = function () {
    const cookie = get_cookie('LWSSO_COOKIE_KEY');
    if (cookie) {
        console.log(pluginName, 'Cookie found, getting id.');
        const data = {
            'viewInfoId': schemeAll,
            'inConsole': true,
            'enableTimeout': true
        };
        VanillaJs.Ajax.Post(nnmPath + 'view', data, function (err, result) {
            if (err) {
                errorGetId(err);
            } else {
                successGetId(JSON.parse(result));
            }
        });
    }
    else {
        console.log(pluginName, 'Cookie not found, timeout!.');
        ready();
    }
};

const ready = function () {
    setTimeout(main, delay);
};

ready();