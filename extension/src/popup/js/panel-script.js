'use strict';
window.browser = (typeof browser !== 'undefined' ? browser : chrome);

const background = browser.extension.getBackgroundPage();
const nodesContainer = document.getElementById('nodesContainer');

const generateAlarmsChecks = function (alarm, value) {
    const checkbox = document.getElementById(alarm);
    if (value) {
        checkbox.setAttribute('checked', '');
    }
    checkbox.addEventListener('click', function () {
        console.log(alarm, 'is', alarm);
        const value = this.checked;
        background.setAlertLevel(alarm, value);
    });
};

const createNode = function(text, value) {
    const input = document.createElement('input');
    input.setAttribute('type', 'checkbox');
    if (value) {
        input.setAttribute('checked', '');
    }
    input.addEventListener('click', function () {
        background.setNodeMode(text, !!this.checked);
        console.log(text, 'monitoring is', node.IsMonitoring);
    });

    const label = document.createElement('label');
    label.className = 'pure-checkbox';
    label.appendChild(input);

    const span = document.createElement('span');
    span.className = 'span-left-margin';
    const textNode = document.createTextNode(text);
    span.appendChild(textNode);
    label.appendChild(span);

    const div = document.createElement('div');
    div.className = 'div-block';
    div.appendChild(label);

    nodesContainer.appendChild(div);
};

const ready = function () {
    const config = background.getConfig();

    generateAlarmsChecks('IsEnabled', config.IsEnabled);
    generateAlarmsChecks('Minor', config.Minor);
    generateAlarmsChecks('Major', config.Major);
    generateAlarmsChecks('Critical', config.Critical);

    for (let nodeText in config.Nodes) {
        createNode(nodeText, config.Nodes[nodeText].IsMonitoring);
    }
};

document.addEventListener("DOMContentLoaded", ready);