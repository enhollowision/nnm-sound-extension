browser = typeof browser !== 'undefined' ? browser : chrome;

const config = {
    'IsEnabled': true,
    'Minor': true,
    'Major': true,
    'Critical': true,
    'Nodes': {}
};

function getConfig() {
    return config;
}

function setAlertLevel(level, value) {
    if (typeof config[level] !== 'undefined' && level !== 'Nodes') {
        config[level] = value;
    }
}

function setNodeMode(node, value) {
    if (typeof config.Nodes[node] !== 'undefined') {
        config.Nodes[node].IsMonitoring = value;
    }
}

function checkNodes(nodes) {
    console.log('Processing nodes array.');
    nodes.forEach(function (item) {
        const text = item.label;
        if (typeof config.Nodes[text] === 'undefined') {
            console.log('Creating new node:', text);
            config.Nodes[text] = {
                'IsMonitoring': true
            }
        }
    });
}

function checkStatusColor(statusColor) {
    switch (statusColor) {
        case 'rgb(148,207,101)':
            // green
            return false;
        case 'rgb(255,222,83)':
            // minor - yellow
            return config.Minor;
        case 'rgb(255,148,40)':
            // major
            return config.Major;
        case 'rgb(254,0,0)':
            // red
            return config.Critical;
        default:
            // other...
            console.error('Unknown statusColor', statusColor);
    }
}

function alertChecking(nodes) {
    if (!config.IsEnabled) {
        return false;
    }
    for (let index = 0; index < nodes.length; ++index) {
        const text = nodes[index].label;
        if (!config.Nodes[text].IsMonitoring) {
            continue;
        }
        const statusColor = nodes[index].statusColor;
        if (checkStatusColor(statusColor)) {
            return true;
        }
    }
    return false;
}

function notify(message, sender, sendResponse) {
    switch (message.type) {
        case 'should_alert':
            checkNodes(message.nodes);
            sendResponse({
                'type': 'should_alert',
                'data': alertChecking(message.nodes)
            });
            break;
    }
}

browser.runtime.onMessage.addListener(notify);