# NNM-Sounds

Add playing sounds on alert in HP NNM

## Building

There is a Grunt script for building addon. So for building you have to install `grunt-cli`
and `web-ext`. Just type command:

```sh
$ npm install grunt-cli web-ext -g
```

After this install all needed packages and type:

```sh
$ npm install
```

For building addon run command:

```sh
$ grunt
or
$ grunt build
```