'use strict';

module.exports = function (grunt) {

    const htmlmind_opt = {
        removeComments: true,
        collapseWhitespace: true,
        removeCommentsFromCDATA: true,
        removeCDATASectionsFromCDATA: true
    };

    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        clean: {
            firefox_build: ['firefox_build'],
            chrome_build: ['chrome_build']
        },
        copy: {
            resources: {
                files: [
                    {expand: true, cwd: 'extension/resources', src: ['*.ogg'], dest: 'firefox_build/resources'}
                ]
            },
            firefox_to_chrome: {
                files: [
                    {expand: true, cwd: 'firefox_build', src: ['**'], dest: 'chrome_build/'}
                ]
            },
            chrome_icons: {
                files: [
                    {expand: true, cwd: 'extension/resources', src: ['*.png'], dest: 'chrome_build/resources'}
                ]
            }
        },
        cssmin: {
            pure: {
                options: {
                    level: {
                        1: {
                            specialComments: 0
                        }
                    }
                },
                files: [
                    {'firefox_build/src/popup/css/inner.css': ['extension/src/popup/css/inner.css']},
                    {'firefox_build/src/popup/css/base.css': ['extension/src/popup/css/base.css']}
                ]
            }
        },
        exec: {
            web_ext_firefox_lint: {
                cmd: 'web-ext lint',
                cwd: 'firefox_build'
            },
            web_ext_firefox_sign: {
                cmd: 'web-ext sign --api-key $AMO_API_KEY --api-secret $AMO_API_SECRET',
                cwd: 'firefox_build'
            },
            web_ext_chrome_zip: {
                cmd: 'web-ext build',
                cwd: 'chrome_build'
            }
        },
        htmlmin: {
            popup: {
                options: htmlmind_opt,
                files: [
                    {'firefox_build/src/popup/index.html': 'extension/src/popup/index.html'}
                ]
            },
            icon_svg: {
                options: htmlmind_opt,
                files: [
                    {'firefox_build/resources/icon.svg': 'extension/resources/icon.svg'}
                ]
            }
        },
        mkdir: {
            build: {
                options: {
                    create: ['firefox_build', 'firefox_build/src', 'firefox_build/src/background',
                        'firefox_build/src/content', 'firefox_build/src/popup/js',
                        'chrome_build']
                }
            }
        },
        uglify: {
            js: {
                files: [{
                    'firefox_build/src/background/index.js' : 'extension/src/background/index.js',
                    'firefox_build/src/content/index.js' : 'extension/src/content/index.js',
                    'firefox_build/src/popup/js/panel-script.js' : 'extension/src/popup/js/panel-script.js'
                }]
            }
        }
    });

    grunt.loadNpmTasks('grunt-contrib-clean');
    grunt.loadNpmTasks('grunt-mkdir');
    grunt.loadNpmTasks('grunt-exec');
    grunt.loadNpmTasks('grunt-contrib-copy');
    grunt.loadNpmTasks('grunt-contrib-uglify-es');
    grunt.loadNpmTasks('grunt-contrib-cssmin');
    grunt.loadNpmTasks('grunt-contrib-htmlmin');

    const getBuild = function () {
        const now = new Date();
        return now.getFullYear() + '.' + (now.getMonth() + 1) + now.getDate() + '.' + now.getHours() + now.getMinutes();
    };

    const getVersion = function (version) {
        const major = version;
        return major + '.' + build;
    };

    const build = getBuild();

    grunt.registerTask('package:firefox_manifest', 'Firefox manifest generation.', function () {
        const path = require('path');
        const manifestPath = path.join('extension', 'manifest.json');
        const firefoxManifestPath = path.join('firefox_build', 'manifest.json');
        const manifestJson = grunt.file.readJSON(manifestPath);
        manifestJson.version = getVersion(manifestJson.version);
        grunt.file.write(firefoxManifestPath, JSON.stringify(manifestJson));
        grunt.log.writeln('Firefox manifest.json version was updated to: ' + manifestJson.version);
    });

    grunt.registerTask('package:chrome_build', 'Chrome manifest generation.', function () {
        const path = require('path');
        const manifestPath = path.join('extension', 'manifest.json');
        const chromeManifestPath = path.join('chrome_build', 'manifest.json');
        const manifestJson = grunt.file.readJSON(manifestPath);
        manifestJson.version = getVersion(manifestJson.version);

        manifestJson.icons['48'] = 'resources/icon48.png';
        manifestJson.icons['96'] = 'resources/icon96.png';

        manifestJson.browser_action.default_icon = {
            "16": "resources/icon16.png",
            "24": "resources/icon24.png",
            "32": "resources/icon32.png"
        };

        delete manifestJson.applications;

        grunt.file.write(chromeManifestPath, JSON.stringify(manifestJson));
        grunt.log.writeln('Chrome manifest.json version was updated to: ' + manifestJson.version);
    });

    grunt.registerTask('prepare:package', ['uglify:js', 'cssmin:pure', 'htmlmin:popup', 'copy:resources',
        'copy:firefox_to_chrome', 'htmlmin:icon_svg', 'copy:chrome_icons',
        'package:firefox_manifest', 'package:chrome_build']);

    grunt.registerTask('make_zip_extensions', ['exec']);

    grunt.registerTask('build', ['clean', 'mkdir:build', 'prepare:package', 'make_zip_extensions']);

    // Default task
    grunt.registerTask('default', ['build']);
};
